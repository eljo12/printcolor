#pragma once
#include <iostream>
#include <windows.h>
#include <ctime> 


inline std::ostream& randomColor(std::ostream& s)
{
    srand((unsigned)time(0));
    int colorCode = (rand() % 15) + 1;
    
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, colorCode);
    return s;
}

inline std::ostream& blue(std::ostream& s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE
        | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
    return s;
}

inline std::ostream& red(std::ostream& s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout,
        FOREGROUND_RED | FOREGROUND_INTENSITY);
    return s;
}

inline std::ostream& errorMsg(std::ostream& s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout,
        12);
    return s;
}

inline std::ostream& green(std::ostream& s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout,
        FOREGROUND_GREEN | FOREGROUND_INTENSITY);
    return s;
}

inline std::ostream& yellow(std::ostream& s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout,
        FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
    return s;
}

inline std::ostream& white(std::ostream& s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout,
        FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
    return s;
}

inline std::ostream& boldYellowTitle(std::ostream& s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout,224);
    return s;
}

inline std::ostream& tab(std::ostream& s)
{
    std::cout << "\t|";
    return s;
}

inline std::ostream& tabl(std::ostream& s)
{
    std::cout << "\t|\n";
    return s;
}

inline std::ostream& tabs(std::ostream& s)
{
    std::cout << "|";
    return s;
}

inline std::ostream& endl (std::ostream& s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout,
        FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
    std::cout << "\n";
    return s;
}

inline std::ostream& endcd(std::ostream& s)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout,
        FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
    return s;
}

struct color {
    color(WORD attribute) :m_color(attribute) {};
    WORD m_color;
};

template <class _Elem, class _Traits>
std::basic_ostream<_Elem, _Traits>&
operator<<(std::basic_ostream<_Elem, _Traits>& i, color& c)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hStdout, c.m_color);
    return i;
}

class loadingBar
{
public:
    loadingBar(int fullSize);
    void printBar();
    void updateBar(int corectSize);
    void updateAndPrint(int corectSize);
    ~loadingBar();

private:
    int _fullSize;
    int _corectSize;
};

loadingBar::loadingBar(int fullSize)
{
    _fullSize = fullSize;
}

inline void loadingBar::printBar()
{
    int print = 0;
    int blank = 0;
}

inline void loadingBar::updateBar(int corectSize)
{
    _corectSize = corectSize;
}

inline void loadingBar::updateAndPrint(int corectSize)
{
    _corectSize = corectSize;
    auto num = _corectSize / _fullSize * 100;
    std::cout << "[";
    std::cout << _corectSize/10;
    for (size_t i = 0; i < num; i++)
    {
        std::cout << "#";
    }
    for (size_t i = 0; i < 100-num; i++)
    {
        std::cout << " ";
    }

    std::cout << "]";
}

loadingBar::~loadingBar()
{

}
